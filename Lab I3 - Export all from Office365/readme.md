# Welcome

Welcome to the lab in which we will be trying to export all sorts of interesting data from your Office365 tenant.
This lab has been prepared for the tenant: M365x724792

Credentials we will be using:
username: admin@M365x724792.onmicrosoft.com
password: gt5SqnBP3CLUbkDC

# Goals
Learning goals of this course are:
- How to export data towards multiple workbooks within an excelsheet
- How to interact with various Office365 services

# Prerequisites
You will need to install a couple of things:
1. You will need to install WMF 5.1. https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure
This is only required for Windows 7 or 8.x users as WMF 5.1 comes in-box with Windows 10.

2. Install the SharePoint Online module. Go to the subfolder "SharePoint Online" and install the MSI over there. Bits were downloaded from the following url: https://www.microsoft.com/en-us/download/details.aspx?id=35588

3. Run the script "Get-Everything.ps1" with the parameter -installmodules.
Open up the script in ISE and press play. It will help you setting up the current working directory.
Then, switch to the Console (Ctrl-D), and hit ".\Get-Everything.ps1 -installmodules"  (without the quotes)

You are done setting up the prerequisites.


# Exercise
Let's go export everything and watch how it goes!
Go to the console (Ctrl-D) and hit ".\Get-Everything.ps1 -ExportAll"

First you will need to enter the credentials (see the top of this readme.md file).
Then the script is good to go to execute all kinds of exports.
Take a look at the ps1 and start playing with it.

You can also log into the Office365 admin portal to explore through the web ui:
https://portal.office.com/ and select admin from the launcher.
Or go straight to: 
https://M365x724792-admin.sharepoint.com