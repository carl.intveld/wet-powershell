# https://www.powershellgallery.com/
# https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure
# -> https://go.microsoft.com/fwlink/?linkid=839516

# https://www.powershellgallery.com/packages/xWebAdministration/1.18.0.0
# Inspect:
# Save-Module -Name xWebAdministration -Path <path> -RequiredVersion 1.18.0.0
# Install:
# Install-Module -Name xWebAdministration -RequiredVersion 1.18.0.0

Configuration Example
{
    Import-DscResource -ModuleName "PSDesiredStateConfiguration"
    Import-DscResource -ModuleName xWebAdministration
    
    <#
    xWebApplication App1
    {
        Ensure                  = "Present"
        Name                    = "app1"
        WebAppPool              = "DefaultAppPool"
        Website                 = "Default Web Site/services"
        PreloadEnabled          = $true
        ServiceAutoStartEnabled = $true
        PhysicalPath            = "C:\inetpub\wwwroot\services\app1"
        #DependsOn               = "[File]Services"
    }
    #>

    # voorbeeld:https://github.com/PowerShell/xWebAdministration/issues/294

        xWebApplication App1
    {
        Ensure                  = "Present"
        Name                    = "app1"
        WebAppPool              = "DefaultAppPool"
        Website                 = "Default Web Site"
        PreloadEnabled          = $true
        ServiceAutoStartEnabled = $true
        PhysicalPath            = "C:\inetpub\wwwroot\New folder"
        #DependsOn               = "[File]Services"
         
    }

}

Example -ConfigurationData $features

# https://blogs.msdn.microsoft.com/powershell/2013/11/26/push-and-pull-configuration-modes/
# Start-DscConfiguration -Path Example -ComputerName localhost -Wait -Verbose
# Test-DscConfiguration -Verbose -Path Configurations -ComputerName localhost # -Verbose
# Test-DscConfiguration -Verbose -Path Example -ComputerName localhost # -Verbose

# https://msdn.microsoft.com/en-us/library/dd878288(v=vs.85).aspx
# Receive-Job -Name Job5
# Get-Job