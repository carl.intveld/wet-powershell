﻿param (
    [switch]$installmodules,
    [switch]$exportall
)

# Prerequisites:
# Install the SharePoint Online Management Shell:
# Go to subfolder "SharePoint Online"
# bits downloaded from https://www.microsoft.com/en-us/download/details.aspx?id=35588
#
# Run the script once with the -installmodules setting to get the required modules installed from the PowerShell gallery



if ($psise -ne $null) {    
    $psisepath = split-path $psise.CurrentFile.FullPath

    $currentpath = (Resolve-Path .\).Path
    if ($currentpath -ne $psisepath) {
        Read-Host "PSISE feature - Press enter to switch current path towards the path of the active script"
        cd $psisepath
    }
}



                #############################
                #                           #
                # Get EVERYTHING Office 365 #
                #                           #
                #############################

if ($installmodules -eq $true) {
    Write-Host -ForegroundColor Green Installing required modules from the PowerShell Gallery. WMF 5.1 required. Download from https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure
#import all modules
install-module MSOnline

#install Excel module from github
Install-Module ImportExcel
exit
}

if ($exportall -eq $false) {
    Write-Host "Welcome to `"Get-Everything.ps1`". Please use one of the following parameters:"
    Write-Host "-InstallModules            Only required as a prerequisite in order to get the required modules installed"
    Write-Host "-ExportAll                 The core function. It exports all kinds of interesting data towards an excelsheet in c:\temp"
    exit
}

# Run the CreateExoPSSession.ps1 once in order to get access to Exchange Online cmdlets
& '.\Exchange Online\CreateExoPSSession.ps1'
# These bits came from the Exchange Online PowerShell module downloaded from the Exchange Admin Center; using Internet Explorer:
# https://outlook.office365.com/ecp/?rfr=Admin_o365&exsvurl=1&mkt=en-US&Realm=M365x724792.onmicrosoft.com&RpsCsrfState=23923bcc-0935-015c-7697-a9b4c9f0dc1f&wa=wsignin1.0
# -> Hybrid menu option -> Configure PowerShell module (second button) 
# dir $Env:LOCALAPPDATA\Apps\2.0\*\CreateExoPSSession.ps1 -Recurse

exit

#start Transcript
$date = (get-date).ToString("ddMMyyyy")
Start-Transcript -Path "C:\temp\PS_$($date).txt" -append

#Get Credentials
$UserCredential = Get-Credential


#Connect to Office 365
Connect-MsolService -Credential $UserCredential

#Connect to Exchange Online
#$EOSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic –AllowRedirection
#$EOSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Authentication Basic –AllowRedirection
Connect-EXOPSSession -Credential $UserCredential

#Get-PSSession


#$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.protection.outlook.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
#Connect-EXOPSSession

#Import-PSSession $EOSession
#Set-User  admin@M365x724792.onmicrosoft.com -RemotePowerShellEnabled $true

#connect to SharePoint Online
Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
Connect-SPOService -url https://M365x724792-admin.sharepoint.com -Credential $UserCredential

#Logfile
$path = "C:\temp"
$date = get-date
$today = $date.ToString("ddMMyyyy_HHmm")
$filepath = "$($path)\Output_$($today).xlsx" 

                ##########################
                #                        #
                # Azure Active Directory #
                #                        #
                ##########################

#Get all licensed users
Get-MsolUser -all | where{$_.isLicensed -eq $true} | Sort -Property DisplayName | select UserPrincipalname, DisplayName, LastPasswordChangeTimestamp, @{Name="License"; Expression = {$_.licenses.accountskuid}} | Export-Excel $filepath -WorkSheetname "Licensed Users" -AutoSize
write-host "Licensed Users" -ForegroundColor Green

#Get all unlicensed users
Get-MsolUser -all -UnlicensedUsersOnly | Sort -Property DisplayName | select UserPrincipalname, DisplayName, LastPasswordChangeTimestamp | Export-Excel $filepath -WorkSheetname "Unlicensed Users" -AutoSize
write-host "Unlicensed Users" -ForegroundColor Green

#Get all external users
Get-MsolUser -all | Sort -Property SignInName | where{$_.UserPrincipalName -like "*#ext#*"} | select SignInName, UserPrincipalName, DisplayName, WhenCreated | Export-Excel $filepath -WorkSheetname "Externe gebruikers" -AutoSize
write-host "External Users" -ForegroundColor Green

#Get all available licenses
Get-MsolAccountSku | select AccountSkuID, ActiveUnits, ConsumedUnits, LockedOutUnits | Export-Excel $filepath -WorkSheetname "Licenses" -AutoSize
write-host "Available Licenses" -ForegroundColor Green

#Get all available domains
Get-MsolDomain | select Name, Status, Authentication |  Export-Excel $filepath -WorkSheetname "Domains" -AutoSize
write-host "Available Domains" -ForegroundColor Green

#Get all Groups
Get-Msolgroup -all | sort -property GroupType | select GroupType, DisplayName, Description, EmailAddress | Export-Excel $filepath -WorkSheetname "Azure Groups" -AutoSize
write-host "Azure Groups" -ForegroundColor Green

#Get all contacts
Get-Msolcontact -all | sort DisplayName | select DisplayName, EmailAddress | Export-Excel $filepath -WorkSheetname "Contacts" -AutoSize
write-host "Contacts" -ForegroundColor Green

                ###################
                #                 #
                # Exchange Online #
                #                 #
                ###################

#Get all mailboxes
Get-Mailbox | sort DisplayName | select DisplayName, Alias, PrimarySMTPAddress, ArchiveStatus, UsageLocation, WhenMailboxCreated | Export-Excel $filepath -WorkSheetname "Mailboxes" -AutoSize
write-host "Mailboxes" -ForegroundColor Green

#Get all archive mailboxes
Get-Mailbox -Archive | sort DisplayName | select DisplayName, Alias, PrimarySMTPAddress, ArchiveStatus, UsageLocation, WhenMailboxCreated | Export-Excel $filepath -WorkSheetname "Archive mailboxes" -AutoSize
write-host "Archive mailboxes" -ForegroundColor Green

#Get all mailboxes statistics
Get-Mailbox | %{Get-MailboxStatistics $_.alias -WarningAction:SilentlyContinue} | sort DisplayName | select DisplayName, ItemCount, TotalItemSize, LastLogonTime |  Export-Excel $filepath -WorkSheetname "Mailboxes Stats" -AutoSize
write-host "Mailboxes Stats" -ForegroundColor Green

#Get all archive mailboxes statistics
Get-Mailbox| %{Get-MailboxStatistics $_.alias -Archive -ErrorAction:SilentlyContinue -WarningAction:SilentlyContinue} | sort DisplayName |select DisplayName, ItemCount, TotalItemSize | Export-Excel $filepath -WorkSheetname "Archive mailboxes Stats" -AutoSize
write-host "Archive mailboxes Stats" -ForegroundColor Green

                #####################
                #                   #
                # Office 365 Groups #
                #                   #
                #####################

Get-UnifiedGroup | sort DisplayName |select DisplayName, Alias, SharePointSiteUrl, PrimarySmtpAddress | Export-Excel $filepath -WorkSheetname "Office 365 Groups" -AutoSize
write-host "Office 365 Groups" -ForegroundColor Green

                #####################
                #                   #
                # SharePoint Online #
                #                   #
                #####################

#Get SharePoint Sites
Get-SPOSite | sort URL | select URL, Title, LocaleID, Owner, WebsCount, LastContentModifiedDate, StorageUsageCurrent, Template | Export-Excel $filepath -WorkSheetname "SharePoint Sites" -AutoSize
write-host "SharePoint Sites" -ForegroundColor Green

#Get SharePoint Users
get-sposite | %{$site = $_.url; Get-SPOUser -Site $site | select @{Name="URL"; Expression = {$site}}, DisplayName, LoginName} | Export-Excel $filepath -WorkSheetname "SharePoint Users" -AutoSize
write-host "SharePoint Users" -ForegroundColor Green

#stop transcript
Stop-Transcript