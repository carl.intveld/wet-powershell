Welcome to beginners lab 1 of the Introduction to PowerShell Course!

This lab is part of the course that can be found on:
gitlab.com/carl.intveld/wet-powershell/

Please start "ISE" Windows PowerShell Integrated Scripting Environment and open lesson1.ps1. You will find "ISE" in your start menu. Just press "Windows-key ISE" and you start up ISE.


Hotkeys to remember are:
* F8 - Run selection
* F5 - Run complete script; which we are not using during this particular lab session.

During this lab session we will be using F8 extensively in order to run the various snippets.
