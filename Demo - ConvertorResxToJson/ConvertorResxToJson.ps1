﻿
<#


ReadResouceFilesToConvert functie leest alle resx bestanden in de gegeven folder en converteert ze in de json format.

.Example
ReadResouceFilesToConvert ".\start"
#>


function ReadResouceFilesToConvert
{

param
   (
	[Parameter(Mandatory = $true)]
	[string]
	$Directory
    )
 

    if (Test-Path -Path $Directory)
    {
    
	    $files = Get-ChildItem -Path $Directory  *.resx -Recurse | % { $_.FullName } 

        if($files.Length -eq 0){
            Write-Host -ForegroundColor Green  Er zijn geen resource file in $Directory. 
         } else {
 
        $files | start-convertor	
        Write-Host -ForegroundColor Green $files.Count resource file zijn naar json geconverteerd.
        }
    }
    else
    {
	 Write-Host -ForegroundColor Red "Cannot find directory: $Directory"      
    }  

}

Function start-convertor  {
    [cmdletbinding()]  
    Param (
        [parameter(ValueFromPipeline)][string[]]$files       
    ) 

    Process  {    
        foreach ($Resourcefile in $files) {
        $Resourcefile | ToJsonconvertor
        }
    }
}


Function ToJsonconvertor{
[cmdletbinding()]  
    Param (
        [parameter(ValueFromPipeline)][string]$Resourcefile
       
    ) 

  [xml]$XmlDocument = (Get-Content -Path $Resourcefile -Encoding Ascii) -join "`n"
  $items = Select-Xml -Xml $xmldocument -XPath '//data'


$jsonobject = New-Object -TypeName PSCustomObject;
$mijnarray = @();

foreach ($item in $items) {
    $key = $item.Node.name
    $value = @{
        "id" = $item.Node.name;
        "groupId" = ([System.IO.FileInfo]$Resourcefile).BaseName;
        "value" = $item.Node.value;
        "comment" = $item.Node.comment;
    }


    if ((Get-Member -InputObject $jsonobject -Name $key) -eq $null) {
        Add-Member -InputObject $jsonobject -Name $key -Value "" -MemberType NoteProperty
    }
    else {
        Write-Host -ForegroundColor Red Waarschuwing: key bestaat al! en wordt nu overschreven
    }

    $jsonobject."$key" = $value;
    $mijnarray += $value
}

$outputPath = Split-Path -Path $Resourcefile;
$filename = ([System.IO.FileInfo]$Resourcefile).BaseName + '.json'; 

$result = Join-Path $outputPath $filename; 

$mijnarray | ConvertTo-Json  | Format-Json | Out-File $result

}
# end of convertor fucntion

# Formats JSON in a nicer format than the built-in ConvertTo-Json does.
function Format-Json([Parameter(Mandatory, ValueFromPipeline)][String] $json) {
  $indent = 0;
  ($json -Split '\n' |
    % {
      if ($_ -match '[\}\]]') {
        # This line contains  ] or }, decrement the indentation level
        $indent--
      }
      $line = (' ' * $indent * 2) + $_.TrimStart().Replace(':  ', ': ')
      if ($_ -match '[\{\[]') {
        # This line contains [ or {, increment the indentation level
        $indent++
      }
      $line
  }) -Join "`n"
}


#ReadResouceFilesToConvert 



