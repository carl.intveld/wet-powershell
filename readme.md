Welcome to the PowerShell workshop!

We have prepared a couple of labs for you whether you are a beginner or an intermediate.


Labs beginning with the character B are meant for beginners.  
Labs beginning with the character I are meant for intermediates.

Good luck!


You can reach out to us by e-mail:  
niloufar.markazi@capgemini.com  
carl.in.t.veld@capgemini.com