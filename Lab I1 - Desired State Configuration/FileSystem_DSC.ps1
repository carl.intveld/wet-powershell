# This script is part of the Introduction to PowerShell course - https://gitlab.com/carl.intveld/wet-powershell
# To run this script you will need to install WMF 5.1 (for Windows 10 this comes out-of-the-box):

# https://www.powershellgallery.com/
# https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure

# And you will need to install the xWebAdministration module:

# https://www.powershellgallery.com/packages/xWebAdministration
# Install-Module -Name xWebAdministration 

param (
    [switch]$Build = $false,
    [switch]$Run = $false,
    [switch]$Test = $false
)

if ($psise -ne $null) {    
    $psisepath = split-path $psise.CurrentFile.FullPath

    $currentpath = (Resolve-Path .\).Path
    if ($currentpath -ne $psisepath) {
        Read-Host "PSISE feature - Druk op enter om current path te zetten op het pad van het script"
        cd $psisepath
    }
}

Write-Host -ForegroundColor White "DSC-Script voor FileSystem"
if (!$Build -and !$Run -and !$Test) {
    Write-Host "Gebruik een of meer van de volgende parameters:"
    Write-Host "-Build    Genereert de DSC configuratie"
    Write-Host "-Test     Test de DSC configuratie"
    Write-Host "-Run      Past de DSC configuratie toe"
    exit;
}

if ($build) {

Configuration DSC_FileSystem
{
    Import-DscResource -ModuleName "PSDesiredStateConfiguration"
    Import-DscResource -ModuleName xWebAdministration

    <#
    xWebApplication App1
    {
        Ensure                  = "Present"
        Name                    = "app1"
        WebAppPool              = "DefaultAppPool"
        Website                 = "Default Web Site/services"
        PreloadEnabled          = $true
        ServiceAutoStartEnabled = $true
        PhysicalPath            = "C:\inetpub\wwwroot\services\app1"
        #DependsOn               = "[File]Services"
    }
    #>


    $AllNodes.Folders.ForEach({
        File $_.Name {
                Type = 'Directory'
                DestinationPath = $_.Folder
                Ensure = "Present"
            }
    })
}

$configurationData = @{
    AllNodes = @(
        @{
            NodeName = "localhost"
            Folders = @( 
                @{
                    Name = "TestUser3"
                    Folder = "C:\TestUser3"
                },
                @{
                    Name = "TestUser4"
                    Folder = "C:\TestUser4"
                }
            )
        }
    )
};

 DSC_FileSystem -ConfigurationData $configurationData -OutputPath output\DSC_FileSystem

# https://blogs.msdn.microsoft.com/powershell/2013/11/26/push-and-pull-configuration-modes/
# Start-DscConfiguration -Path Example -ComputerName localhost -Wait -Verbose
# Start-DscConfiguration -Path FileSystemEDVKS -ComputerName localhost -Wait -Verbose

# https://msdn.microsoft.com/en-us/library/dd878288(v=vs.85).aspx
# Receive-Job -Name Job5
# Get-Job

# Test-DscConfiguration -Verbose -Path FileSystemEDVKS -ComputerName localhost

}

if ($test) {
    Test-DscConfiguration -Verbose -Path output\DSC_FileSystem -ComputerName localhost
}

if ($run) {
    Start-DscConfiguration -Path output\DSC_FileSystem -ComputerName localhost -Wait -Force -Verbose
}