# Geschreven door Carl in 't Veld - april 2018
# https://blog.kloud.com.au/2016/04/21/using-saved-credentials-securely-in-powershell-scripts/
# Om vanuit ISE te kunnen debuggen: $path = split-path $psise.CurrentFile.FullPath
$path = split-path -Path $PSCommandPath 
$pathtoRDPFILE = Join-Path -Path $path -ChildPath "rdpprofile.rdp"
$pathtocredentials = Join-Path -Path $path -ChildPath ".credentials.json"
$pathtohostfile = Join-Path -Path $path -ChildPath "hosts.json"

$current = 1
$hosts = (Get-Content $pathtohostfile) -join "`n" | ConvertFrom-Json

function NieuwWachtwoord($credentialskey) {
    Write-Host -ForegroundColor White Credentialkey = $credentialskey

    if ($(Test-Path $pathtocredentials)) {
        $credentialscollection = (Get-Content $pathtocredentials) -join "`n" | ConvertFrom-Json
        $credentials = $credentialscollection."$credentialskey"
    }
    else {
        $credentialscollection = @{};
    }
    

    # Uitvragen username
    if ($credentials -eq $null) {
        $username = Read-Host Wat is je username?
    } else {
        $username = Read-Host "Wat is je username? (leeg voor "$($credentials.Username)")"
        if ($username -eq "") {
            $username = $credentials.Username
        }
    }

    # Uitvragen wachtwoord
    if ($credentials -eq $null) {
        $securepassword = Read-Host -AsSecureString Wat is je wachtwoord? 
    } else {
        $securepassword = Read-Host -AsSecureString "Wat is je wachtwoord? (leeg voor bestaande wachtwoord)"
        if ($securepassword.Length -eq 0) {
            $securepassword = $Credentials.Password | ConvertTo-SecureString
        }
    }

    $credentials = @{
        Username = $username;
        Password = $(ConvertFrom-SecureString $securepassword)
    }

    if ($credentialscollection."$credentialskey" -eq $null) {
        Add-Member -InputObject $credentialscollection -Name $credentialskey -Value "" -MemberType NoteProperty        
    } 
    $credentialscollection."$credentialskey" = $credentials;

    #Set-Content $filepath $(ConvertFrom-SecureString $securepassword)
    $credentialscollection | ConvertTo-Json | Out-File $pathtocredentials
}


function StartTSC($current) {
    $hostitem = $hosts[$current-1]

    if ($(Test-Path $pathtocredentials)) {
        # haal huidig wachtwoord op    
        $credentialscollection = (Get-Content $pathtocredentials) -join "`n" | ConvertFrom-Json
        $credentials = $credentialscollection."$($hostitem.account)"
        if ($credentials -ne $null) {
            $securepassword = $credentials.Password | ConvertTo-SecureString
            $UnsecurePassword = (New-Object PSCredential "user", $SecurePassword).GetNetworkCredential().Password
            cmdkey /generic:"TERMSRV/$($hostitem.IP)" /user:$($credentials.Username) /pass:"$UnsecurePassword"    
        }
        else {
            Write-Host -ForegroundColor Red Credentials voor $($hostitem.account) niet aanwezig in credentials-file. Kan credentials niet opslaan in credentials manager.
        }
        
    } else {
        Write-Host -ForegroundColor Red Kan credentials-file niet openen: $pathtocredentials. Kan credentials niet opslaan in credentials manager.
    }
    mstsc $pathtoRDPFILE /v:$($hostitem.IP)
    Write-Host "Mstsc opgestart. rdpfile: $pathtocredentials; hostname: $($hostitem.IP); account: $($hostitem.account)"
}

do {
    Write-Host
    Write-Host "Remote desktop connector"
    Write-Host "--------------------"
    Write-Host
    Write-Host "<enter> Connect to $current"

    for ($i = 0; $i -lt $hosts.Length; $i++) {
        Write-Host $(($i+1).ToString() + ": " + $hosts[$i].IP + " (" + $hosts[$i].Name + ")")
    }

    Write-Host "RD) Set DEV.LOCAL account"
    Write-Host "RO) Set ONTWIKKEL.HRC account"
    Write-Host "RDT) Set TEST.LOCAL account"
    Write-Host "RT) Set TEST.HRC account"
    Write-Host "RP) Set PROD account"
    Write-Host "<Ctrl-c> Abort"

    $choice = Read-Host Make a choice
    [int]$returnedInt = 0
    [bool]$result = [int]::TryParse($choice, [ref]$returnedInt)
    if ($result) {
        $current = $choice
        StartTSC $current
    }
    elseif ($choice -eq "dev") {
        NieuwWachtwoord "dev"
    }
    elseif ($choice -eq "prod") {
        NieuwWachtwoord "prod"
    }
    else {    
        StartTSC $current
    }
}
until ($false)
# /w:3000 /h:1000

# ip-nummers te testen
# UWV OTOD gateway: 10.61.3.1 
# UWV proxy: 10.122.74.1
# jump-server O: 10.145.65.9
# 

exit
# OUDE SPULLEN

# Decoding your mime64 password into string
Split-Path $script:MyInvocation.MyCommand.Path
get-scriptdirectory
$global:MyInvocation.MyCommand.Path

$base64 = "UABhAHMAcwB3AG8AcgBkADAAMQAyAA=="
$Pass= [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($base64))

# Encoding your password into mime64
$Text = ‘<new password>’
$Bytes = [System.Text.Encoding]::Unicode.GetBytes($Text)
$EncodedText =[Convert]::ToBase64String($Bytes)
$EncodedText

route add 10.145.65.9 mask 255.255.255.0 10.78.8.1 metric 2 if 8
