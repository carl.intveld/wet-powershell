# Desired state configuration (DSC)

# DSC in general
DSC is Microsoft's lower layer in the infrastructure provisioning. It does not replace tools such as Chef and Puppet, it simply enables these tools to perform provisioning on the Windows platform. 
DSC supports two models: push and pull:
1. Push means that we as an Operator push some configuration towards a node. This node then picks this up and will apply the requested desired state to the node.
2. Pull means that the node will pull the desired state configuration from a central storage periodically. 

During this lab we will be using the push model solely.

# Context
The Desired state configuration (DSC) scripts in this folder should take care of configuring a VM.

# Preparations
Two items:

1. You need to have WMF 5.1 installed in order to run the scripts.
Go to: https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure
Windows 10 comes with WMF 5.1 out-of-the-box.

2. Install the xWebAdministration DSC module from the PowerShell online Gallery:
Install-Module -Name xWebAdministration
This must be run from an elevated prompt.

# Goals
During this lab your learning goals are the following:
* get acquainted with DSC 
* build out your own DSC files

# Steps
Step 1.
Open up FileSystem_DSC.ps1. And run it.

Step 2.
From the prompt type ".\FileSystem_DSC.ps1 -build"

Key point to learn here is that a DSC block is just a function that needs to be executed. This results in a MOF file. MOF is an open standard: Managed Object Format. Refer to https://en.wikipedia.org/wiki/Common_Information_Model_(computing)

Step 3.
From the prompt type ".\FileSystem_DSC.ps1 -test"
And inspect the report.

Key point to learn is that you can evaluate a DSC block without really executing it.

Step 4.
From the prompt type ".\FileSystem_DSC.ps1 -run"
And inspect the report.

Confirm that the desired folders have been created. 
And confirm that you can execute this script endlessly without side-effects as you really are describing the Desired State which does not change.

Step 5.
Go wild and try out yourself. Inspect OS_DSC.ps1 and IIS_DSC.ps1 as well. These are not yet as polished as FileSystem_DSC.ps1.
Consult the DSC web resources to get acquainted with additional applications.
