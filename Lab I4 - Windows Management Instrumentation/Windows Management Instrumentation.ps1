﻿param (
    [switch]$install,
    [switch]$deinstall
)

if ($psise -ne $null) {    
    $psisepath = split-path $psise.CurrentFile.FullPath

    $currentpath = (Resolve-Path .\).Path
    if ($currentpath -ne $psisepath) {
        Read-Host "PSISE feature - Press enter to switch current path towards the path of the active script"
        cd $psisepath
    }
}


if ($install -eq $false -and $deinstall -eq $false) {
    echo "-install           To register your callback" 
    echo "-deinstall         To unregister your callback" 
    exit
}

# Refer to https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-powershell-1.0/ff730927(v=technet.10)
# for documentation on "WMI Event Monitoring"

if ($install) {

    $ActionBlock = {            
     $e = $event.SourceEventArgs.NewEvent.TargetInstance    
     # https://msdn.microsoft.com/en-us/library/aa394372%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396
     $CurrentDate = Get-Date
     $ProcessName = $(Get-Process -Id $e.ParentProcessId).ProcessName
     $win32_services = get-wmiobject Win32_service
     $services_parent = ($win32_services | where ProcessId -eq $e.ParentProcessId | select -ExpandProperty Name) -join ","
     if ($services_parent -ne "") {
        $services_parent = " ($services_parent)"
     }
     $services_child = ($win32_services | where ProcessId -eq $e.ProcessID | select -ExpandProperty Name) -join ","
     if ($services_child -ne "") {
        $services_child = " ($services_child)"
     }
     write-host ("{0} Parent {1} with PID {2}{3} started {4} with PID {5}{6}" -f $currentDate, $ProcessName, $e.ParentProcessId, $services_parent, $e.Name, $e.ProcessID, $services_child) 
    }   

    Register-WMIEvent -query "Select * From __InstanceCreationEvent within 3 Where TargetInstance ISA 'Win32_Process'" `
    -sourceIdentifier "NewProcess" -action $ActionBlock

    exit
}

if ($deinstall) {
    Unregister-Event "NewProcess"
    exit
}


# following are some randomly collected snippets to get all this stuff working. use at your own risk :)

Get-EventSubscriber
Unregister-Event "NewProcess-Test"
Register-WMIEvent -query "Select * From __InstanceCreationEvent within 3 Where TargetInstance ISA 'Win32_Process'" `
-sourceIdentifier "NewProcess-Test" -action {Write-Host "A new process has started."}


# Possibly interesting for you is the following blog:
# http://blogs.technet.com/heyscriptingguy/archive/2005/03/21/how-can-i-be-notified-any-time-a-network-cable-gets-unplugged.aspx



Register-WMIEvent -query "Select * from MSNdis_StatusMediaDisconnect" `
-sourceIdentifier "NetworkDisconnect" -action {Write-Host "Media disconnected"} -Namespace "ROOT\WMI"

Register-WMIEvent -query "Select * from MSNdis_StatusNetworkChange" `
-sourceIdentifier "NetworkChange" -action {Write-Host "Network changed"} -Namespace "ROOT\WMI"

Register-WMIEvent -query "Select * from MSNdis_StatusMediaConnect" `
-sourceIdentifier "NetworkConnect" -action {Write-Host "Media2 connected"} -Namespace "ROOT\WMI"



http://wutils.com/wmi/root/wmi/wmievent/


https://blogs.technet.microsoft.com/askperf/2014/08/11/wmi-missing-or-failing-wmi-providers-or-invalid-wmi-class/
wmimgmt.msc
wmimgmt.exe
Wbemtest 

$(Get-Process -Id 5468).ProcessName

get-date

$process = get-process -id 23492
$process
Get-Service
get-wmiobject Win32_service | where Started -eq "True" | select Name, ProcessId
get-wmiobject Win32_service | where {$_.Started -eq "True" -and $_.ServiceType -eq "Share Process"}  |   select Name, ProcessId
(get-wmiobject Win32_service | where ProcessId -eq 1072 | select -ExpandProperty Name) -join ","
$service = Get-Service -Name vpnagent

(Get-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings').proxyServer
[System.Net.WebProxy]::GetDefaultProxy() | select address

Register-WMIEvent -query "Select * From __InstanceCreationEvent within 3 Where TargetInstance ISA 'Win32_Process'" `
-messageData "A new process has started." -sourceIdentifier "New Process"

get-event

