Welcome to lab 2 of the intermediate track!
This lab is part of the Introduction to PowerShell course - https://gitlab.com/carl.intveld/wet-powershell

Goal of this lab:
1. Learn building menu applications in PowerShell
2. Serializing / deserializing json
3. Building out your own persistence

Steps:

Step 1.
Open up goto-rdp.ps1 in "ISE". And play with it.

Step 2.
Try out the persistence of "dev" and "prod" account.
Confirm in the current ps1 code that these accounts are hardcoded.

Assignment:
Build out the ps1 code so that the accounts are read from a json-file.
