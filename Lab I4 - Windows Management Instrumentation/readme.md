Welcome to the lab with regards to Windows Management Instrumentation!

This lab belongs to the "Introduction to PowerShell" - https://gitlab.com/carl.intveld/wet-powershell/

In this lab you will learn:
* How to get a callback from the Windows Management Instrumentation layer

Open up Windows Management Instrumentation.ps1 in ISE and press play to get the first instructions on how to execute the script.
