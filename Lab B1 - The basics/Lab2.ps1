﻿#  In this Lab, you will learn about: 
# 
#  Variables
#  Arrays
#  Hashtables
#  Custom Objects
#  Read-Host Write-Host#  Pipes #  Conditions#  Iteration#  Function


# Snippet 1: VARIABLES
# All variables are prefixed with $

$firstname ="John"
$lastname ="Pors"
$fullname ="$firstname $lastname"

Write-Host "Hello $fullname, may I call you $firstname`?"#Forme: We need to use ` between $firstname and ? to avoid ? being “part of” the variable name.$firstname.GetType() # what kind of object is it?
$firstname | Get-Member # Which methods and properties are available?# Snippet 2: Arrays# Arrays are created with @(...)$myArray = @(" linux " , " windows ")
Write-Host $myArray[1] # prints windows
Write-Host $myArray # prints array values

$myArray += @(" mac ")
Write-Host $myArray # prints array values
Write-Host $myArray.Count #prints length of array#If you want to access an array element within an interpolated string, you have to place the array element in parentheses like this:Write-Host "My operating system is $($myArray[1])"# Snippet 3: Hashtables( a dictionary or associative array)# Hashtables are created with @{...}
$user = @{
" jpors " = " John Pors " ;
" pnobel " = " Paul Nobel "
}$user += @{" rveldhuizen " =" Ronald Veldhuizen "}Write-Host $user[" jpors "]          # prints John Pors
Write-Host @user                        # prints array values
Write-Host $user.Keys                   # prints array keys
Write-Host $user.Count                  # prints length of array# Snippet 4: Custom Objects# PowerShell is based on the object-oriented framework .NET, creating and manipulatingvobjects is a world by it self
$myhost = New-Object PSObject  -Property `
 @{ 
    os ="";
    sw =@();
    user =@{} 
 }

$myhost.os = " linux "
$myhost.sw += @(" gcc " ," flex " ," vim ")
$myhost.user += @{
" jpors " =" John Pors " ;
" pnobel "=" Paul Nobel "
}

Write-Host $myhost.os
Write-Host $myhost.sw[2]
Write-Host $myhost.user[" pnobel "]$myhost.GetType()$myhost | Get-Member# Snippet 5: Read-Host / Write-Host$something = Read-Host " Say something here "
Write-Host " you said: " $something#Input from System Commands$name =(Get-WmiObject Win32_OperatingSystem).Name
$kernel =(Get-WmiObject Win32_OperatingSystem).Version
Write-Host "I am running on $name , version $kernel in $( Get-Location )"# snippet 6 Pipe# lists Windows Services which the status is running# Note that the output of Get-Service becomes the input of Where-Object.Get-Service | Where-Object {$_.status -eq 'Running' }# Gets first 10 processes by their Virtual Memory and sort in descending order: get-process | sort -Property "VirtualMemorySize" -Descending | Select -first 10# Snippet 7:  Conditions# if/else$a = Read-Host " Enter a number betrween 0 to 10: "if ($a -gt 2) {
    Write-Host "The value $a is greater than 2."
}
elseif ($a -eq 2) {
    Write-Host "The value $a is equal to 2."
}
else {
    Write-Host ("The value $a is less than 2")
}# Switch/case$short = @{ yes ="y "; nope = "n" }

$ans = Read-Host

switch ( $ans ) {
    y { Write-Host " yes " }
    n { Write-Host " nope "; break }      
    default { Write-Host "$ans `???, type y or n "}
}#Where/Where-ObjectGet-ChildItem | Where-Object {$_.Length -gt 1KB }# Get-WmiObject cmdlet gets instances of Windows Management Instrumentation (WMI) classes or information about the available WMI classes.Get-WmiObject -Class Win32_SystemDriver | Where-Object -FilterScript { ($_.State -eq 'Running') -and ($_.StartMode -eq 'Manual') } | Format-Table -Property Name,DisplayName# Snippet 8:  Iteration# for loop$file = Get-ChildItem
for ($i = 0; $i -lt $file.Count; $i++) {
    if (!( Get-Item $file[$i]).PSIsContainer) {
        Write-Host $file[$i].Name " is a file "
    } else {
        Write-Host $file[$i].Name " is a directory "
    }
}

# foreach
$user =@{
" pnobel " = " Paul Nobel " ;
" jpors " = " John Pors ";
" iveldhuizen " = " Ivan Veldhuizen "
}

foreach ( $key in $user.Keys ) {
Write-Host $user[$key]
}$user =@{
" pnobel " = " Paul Nobel " ;
" jpors " = " John Pors ";
" iveldhuizen " = " Ivan Veldhuizen "
}
# Exercise: rewrite the above foreach loop using Pipe$user.Keys |  foreach { $user.Item($_) }

# While
$file = Get-ChildItem
$i =0
while ($i -lt $file.Count ) {
    if (!( Get-Item $file[$i]).PSIsContainer ) {
        Write-Host $file[$i].Name " is a file "
    } else {
        Write-Host $file[$i].Name " is a directory "
    }
  $i ++
}

# Snippet 9:  Function
# Get-Sum calculates the sum of number between two given numbers.
Function Get-Sum ($a) { 
    Return ($a | Measure-Object -Sum).Sum
 }

Get-Sum (1..36)


# Get-AvgScore calculates the avarage of scores in the games.
# You need to supply three parameters, a name, the number of scores, and the number of games. 
Function Get-AvgScore {
    Param ($Name, $scores, $games)
    $Avg = [int]($scores / $games) 
    Write-Output "$Name's Average score = $Avg, $scores, $games"
}

# use:
Get-AvgScore Paul 9 5