﻿#  In this Lab, you will learn: 
# 
#   Some of the basic PowerShell cmdlets.
#   Create a file, delete a file and change file directory
#   Get help if you needed
#   Basic piping


# Snippet 1: list frist 10 running processes
Get-Process | Select -First 10

# Snippet 2: Clears the display.
Clear-Host

# Snippet 3: Gets the aliases 
Get-Alias

# Snippet 4: cd or Set-Location: Sets the current working location to a specified location.
Set-Location c:

# Snippet 5: dir or Get-ChildItem: Gets the items and child items in one or more specified locations.

# Get all files under the current directory:
Get-ChildItem

# Get all files under the current directory as well as its subdirectories:
dir -Recurse

# List all files with "ps1" file extension.
dir –Path *.ps1 -Recurse

# Snippet 6: New-Item: Creates a new item and adds the phrase "Hello world!"
New-Item -Path ./test.txt -Value "Hello world!" -Force

# Snippet 7: type Or Get-Content: Gets the content of the item at the specified location.
Get-Content -Path ./test.txt

# Snippet 8: del or Remove-Item: Deletes the specified items.
Remove-Item ./test.txt

# Snippet 9: The Get-Help cmdlet shows you how PowerShell commands works with examples
Get-Help -Name Get-Process

Update-Help #to update the help data
Get-Help Get-Process -Examples

# Snippet 10: you’ll get all services sorted by their status
Get-Service | Sort-Object -property Status

# The following script lists all services, with the first pipe excluding stopped services and the second pipe limiting the list to display names only:
Get-Service | WHERE {$_.status -eq "Running"} | SELECT -First 10 displayname
