param (
    [string]$Application,
    [string]$Resource,
    [switch]$TFS,
    [switch]$Nuget,
    [switch]$MSBuild,
    [switch]$IIS,
    [switch]$All,
	[switch]$Force    
)

#
# FUNCTIES
#


# helper to turn PSCustomObject into a list of key/value pairs
function Get-ObjectMembers {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True, ValueFromPipeline=$True)]
        [PSCustomObject]$obj
    )
    $obj | Get-Member -MemberType NoteProperty | ForEach-Object {
        $key = $_.Name
        [PSCustomObject]@{Key = $key; Value = $obj."$key"}
    }
}

function GetApplications {
    $applications | get-objectmembers | % { $_.Key }
    
    #foreach ($app in $applications) {

    #}
}

function ProvisionTFSSourcecode($relativepath) {    
    $tfspath = Join-Path $workspacepath -ChildPath $relativepath
    $tf = "D:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\TF.exe"
    Write-Host -ForegroundColor Cyan "TFS:"
    Write-Host -ForegroundColor Gray $tf vc get /recursive $TFSPath
    & $tf vc get /recursive $TFSPath
}

function ProvisionNuget($tfspath, $relativepath) {
    $solution = Join-Path $workspacepath -ChildPath $tfspath | Join-Path -ChildPath $relativepath
    # nuget.exe is gedownload van https://www.nuget.org/ -- de dll is weliswaar ge�ntegreerd in Visual Studio; de executable echter niet
    $nugetcli = "E:\projects\edv\Project Files\Provisioning\TFS automatisering\nuget.exe"
    Write-Host -ForegroundColor Cyan "Nuget:"
    Write-Host -ForegroundColor Gray $nugetcli restore $Solution
    & $nugetcli restore $Solution
}

function ProvisionMsbuild($tfspath, $relativepath) {
    $solution = Join-Path $workspacepath -ChildPath $tfspath | Join-Path -ChildPath $relativepath
    $msbuildcli = "D:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe"
    Write-Host -ForegroundColor Cyan "Msbuild:"
    Write-Host -ForegroundColor Gray $msbuildcli $Solution
    & $msbuildcli $Solution
}

function ProvisionIIS($tfspath, $relativepath) {
    $appcmd = "$env:SystemRoot\system32\inetsrv\appcmd.exe"    
    $csproj = Join-Path $workspacepath -ChildPath $tfspath | Join-Path -ChildPath $relativepath
    #$csproj = "E:\projects\edv\Statusinformatie WW\DEV\Presentation\Presentation.csproj"
    $physicalfolder = Split-Path $csproj -Parent
    [xml]$XmlDocument = Get-Content -Path $csproj
    # http://schemas.microsoft.com/developer/msbuild/2003
    #$node = $xmldocument.SelectSingleNode("//IISUrl", )
    $ns = @{msbuild = "http://schemas.microsoft.com/developer/msbuild/2003"}
    $items = Select-Xml -Xml $xmldocument -XPath '//msbuild:IISUrl' -Namespace $ns
    $url = [System.Uri]$items.Node.'#text'
    Write-Host -ForegroundColor Cyan "IIS:"

    # de volgende code klapt waarschijnlijk als we dit ook gaan gebruiken voor de rootconfig binding...
    $iisapppath = "IIS:\Sites\$($url.Host)\$($url.Segments[1])"
    $exists = test-path $iisapppath
    if (!$exists) {
        # ter voorbereiding de PowerShell variant:
        # New-WebApplication -Site $WebSiteName -name $WebAppName  -PhysicalPath $AppFolder -ApplicationPool $WebAppName
        # Set-ItemProperty IIS:\AppPools\$WebAppName
        # get-item IIS:\Sites\local.portaalwn.uwv.nl\statusinfoww
        # $item.PhysicalPath
        #$exists = (& $appcmd list app /app.name:"local.portaalwn.uwv.nl/statusinfoww3") -ne $null
        Write-Host -ForegroundColor Gray "$appcmd add app /site.name:$($url.host) /path:$($url.LocalPath) /physicalPath:""$physicalfolder"""
        $AllArgs = @("add", "app", "/site.name:$($url.host)", "/path:$($url.LocalPath)", "/physicalPath:""$physicalfolder""")
        & $appcmd $AllArgs
    } else {
        $commandline = "$appcmd set app /app.name:$($url.host + $url.LocalPath) /[path='/'].physicalPath:'$physicalfolder'"
        Write-Host -ForegroundColor Gray $commandline
        $AllArgs = @("set", "app", "/app.name:$($url.host + $url.LocalPath)", "/[path='/'].physicalPath:""$physicalfolder""") 
        & $appcmd $AllArgs
    }
    $website = Get-Item IIS:\Sites\$($url.Host)
    Set-ItemProperty $iisapppath applicationPool $website.applicationPool
}

function ProvisionIISCollection($tfspath, $collection) {
    foreach ($csprojpath in $collection) {        
        ProvisionIIS $tfspath $csprojpath
    }
}

function CheckHistoryDate($resourcehistory, $key) {
    $value = $resourcehistory."$key"
    if ($value -eq $null) {
        return $true;
    }
    [DateTime]$historydate = [datetime]::parseexact($value, "yyyyMMdd", $null)
    $currentdate = Get-Date
    $b = $currentdate.Date -gt $historydate
    return $b
}

function SetHistoryDate($resourcehistory, $key) {
    if ((Get-Member -InputObject $resourcehistory -Name $key) -eq $null) {
        Add-Member -InputObject $resourcehistory -Name $key -Value "" -MemberType NoteProperty
    }
    $resourcehistory."$key" = (Get-Date).Date.ToString("yyyyMMdd")
}

function ProvisionResource($resourcekey) {
    $resource = $resources."$resourcekey"

    if ($resource -eq $null) {
        Write-Host -ForegroundColor red Resource `"$ResourceKey`" niet gevonden
        return;
    }

    $resourcehistory = $history."$resourcekey"
    if ($resourcehistory -eq $null) {
        $resourcehistory = New-Object -TypeName PSCustomObject
    }

    #$resources."$myresource"

    if ($All -or $TFS) {
        $do = CheckHistoryDate $resourcehistory "TFS"
        if (!$do -and !$Force) {
            Write-Host -ForegroundColor Gray TFS nieuw genoeg. Overslaan
        } else { 
            ProvisionTFSSourcecode $resource.TFS
            SetHistoryDate $resourcehistory "TFS"
        }
    }

    if ($All -or $Nuget) {
        $do = CheckHistoryDate $resourcehistory "Nuget"
        if (!$do -and !$Force) {
            Write-Host -ForegroundColor Gray Nuget nieuw genoeg. Overslaan
        } else { 
            ProvisionNuget $resource.TFS $resource.Solution
            SetHistoryDate $resourcehistory "Nuget"
        }
    }

    if ($All -or $Msbuild) {
        $do = CheckHistoryDate $resourcehistory "MSBuild"
        if (!$do -and !$Force) {
            Write-Host -ForegroundColor Gray MSBuild nieuw genoeg. Overslaan
        } else {            
            ProvisionMsbuild $resource.TFS $resource.Solution
            SetHistoryDate $resourcehistory "MSBuild"
        }
    }

    if ($All -or $IIS) {
        $do = CheckHistoryDate $resourcehistory "IIS"
        if (!$do -and !$Force) {
            Write-Host -ForegroundColor Gray IIS nieuw genoeg. Overslaan
        } else {            
            ProvisionIISCollection $resource.TFS $resource.IIS
            SetHistoryDate $resourcehistory "IIS"
        }
    }

    if ((Get-Member -InputObject $history -Name $resourcekey) -eq $null) {
        Add-Member -InputObject $history -Name $resourcekey -Value "" -MemberType NoteProperty
    }
    $history."$resourcekey" = $resourcehistory;
}


#
#  HIER BEGINT DE RUNTIME
#

Import-Module WebAdministration

$workspacepath = "e:\projects\edv"
$fullpath = (Get-Item -Path "." -Verbose).FullName
$applicationspath = Join-Path $fullpath -ChildPath "applications.json"
$resourcespath = Join-Path $fullpath -ChildPath "resources.json"
$historypath = Join-Path $fullpath -ChildPath ".history"

Write-Host -ForegroundColor White Provision application script. Voert een Get-Latest/Nuget restore/msbuild en IIS mapping uit. Versie 1.0.1

#(Get-Content JsonFile.JSON) -join "`n" | ConvertFrom-Json
if (!(Test-Path $applicationspath)) {
    Write-Host -ForegroundColor Red $applicationspath niet gevonden
    if ($psise -eq $null) {
        exit
    }
    Read-Host "PSISE feature - Druk op enter om current path te zetten op het pad van het script"
    $psisepath = split-path $psise.CurrentFile.FullPath

    $currentpath = (Resolve-Path .\).Path
        if ($currentpath -ne $psisepath) {
        cd $psisepath
    }
    exit;
}
$applications = (Get-Content $applicationspath) | ConvertFrom-Json

if ($Application.Length -gt 0) {
    if ($applications."$Application" -eq $null) {
        Write-Host -ForegroundColor Red Applicatie `"$Application`" niet gevonden
        $Error = $true;
    }
}

if (!(Test-Path $resourcespath)) {
    Write-Host -ForegroundColor Red $resourcespath niet gevonden    
    exit;
}
$resources = (Get-Content $resourcespath) | ConvertFrom-Json

if ($Error -eq $true -or ($application.Length -eq 0 -and $resource.Length -eq 0) -or (!$All -and !$TFS -and !$Nuget -and !$MSBuild -and !$IIS)) {
    echo "Gebruik de volgende parameters:"
    echo "-Application <applicatienaam>"
    echo "-Resource <resourcenaam>"
    echo "-TFS     Haal de laatste versie van broncode op"
    echo "-Nuget   Voer een nuget restore uit"
    echo "-MSBuild Bouw de broncode"
    echo "-IIS     Koppel de applicatie aan IIS"
    echo "-All     Alle provision stappen"
	echo "[-Force] Normaal gesproken worden provisioning stappen alleen dagelijks opnieuw gedaan. Dit kun je overrulen"
    Write-Host "" 
    echo "De volgende applicaties staan geregistreerd:"
    GetApplications | % { Write-Host -ForegroundColor Magenta $_ }
    exit
}

if (Test-Path -Path $historypath) {
    $history = (Get-Content $historypath) | ConvertFrom-Json
} else {
    $history = New-Object -TypeName PSCustomObject
}

if ($Application.Length -eq 0) {
# De gebruiker wil ��n enkele $Resource provisionen:
    Write-Host -ForegroundColor Yellow "Provisioning the following resource: $resource"
    ProvisionResource($resource)
    $history | ConvertTo-Json | Out-File $historypath
    Write-Host -ForegroundColor Yellow Done
    Write-Host "" 
    exit
}

echo "Provisioning the following resources:"
#$myresource = $applications."$application"[0]
$applications."$application" | % { Write-host -ForegroundColor Yellow $_ }
Write-Host ""

foreach ($resource in $applications."$application") {
    Write-Host -ForegroundColor Yellow "Provisioning the following resource: $resource"
    ProvisionResource($resource)
    $history | ConvertTo-Json | Out-File $historypath
    Write-Host -ForegroundColor Yellow Done
    Write-Host "" 
}