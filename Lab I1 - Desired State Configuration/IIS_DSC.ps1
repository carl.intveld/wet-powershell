# https://www.powershellgallery.com/
# https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure
# -> https://go.microsoft.com/fwlink/?linkid=839516

# https://www.powershellgallery.com/packages/xWebAdministration/1.18.0.0
# Inspect:
# Save-Module -Name xWebAdministration -Path <path> -RequiredVersion 1.18.0.0
# Install:
# Install-Module -Name xWebAdministration -RequiredVersion 1.18.0.0

param (
    [switch]$Build = $false,
    [switch]$Run = $false,
    [switch]$Test = $false
)


Write-Host -ForegroundColor White "DSC-Script voor FileSystem"
if (!$Build -and !$Run -and !$Test) {
    Write-Host "Gebruik een of meer van de volgende parameters:"
    Write-Host "-Build    Genereert de DSC configuratie"
    Write-Host "-Test     Test de DSC configuratie"
    Write-Host "-Run      Past de DSC configuratie toe"
    exit;
}

if ($build) {
Configuration EDV_KS_IIS
{
    Import-DscResource -ModuleName "PSDesiredStateConfiguration"
    Import-DscResource -ModuleName xWebAdministration
    
    <#
    xWebApplication App1
    {
        Ensure                  = "Present"
        Name                    = "app1"
        WebAppPool              = "DefaultAppPool"
        Website                 = "Default Web Site/services"
        PreloadEnabled          = $true
        ServiceAutoStartEnabled = $true
        PhysicalPath            = "C:\inetpub\wwwroot\services\app1"
        #DependsOn               = "[File]Services"
    }
    #>

    # voorbeeld:https://github.com/PowerShell/xWebAdministration/issues/294

    xWebSite portaalwn {
        Ensure = "Present"
        Name = "local.portaalwn.uwv.nl"
    }

    xWebSite portaalwn2 {
        Ensure = "Present"
        Name = "local.portaalwn2.uwv.nl"
        BindingInfo     = @(
                            MSFT_xWebBindingInformation
                                 {
                                    Protocol = "http"
                                    HostName = "local.portaalwn2.uwv.nl"
                                 }
                            )
    }

        xWebApplication App1
    {
        Ensure                  = "Present"
        Name                    = "app1"
        WebAppPool              = "DefaultAppPool"
        Website                 = "Default Web Site"
        PreloadEnabled          = $true
        ServiceAutoStartEnabled = $true
        PhysicalPath            = "C:\inetpub\wwwroot\New folder"
        #DependsOn               = "[File]Services"
         
    }

}

# EDV_KS_IIS -ConfigurationData $features
EDV_KS_IIS -OutputPath output\EDV_KS_IIS

# https://blogs.msdn.microsoft.com/powershell/2013/11/26/push-and-pull-configuration-modes/
# Start-DscConfiguration -Path Example -ComputerName localhost -Wait -Verbose
# Test-DscConfiguration -Verbose -Path Configurations -ComputerName localhost # -Verbose
# Test-DscConfiguration -Verbose -Path Example -ComputerName localhost # -Verbose

# https://msdn.microsoft.com/en-us/library/dd878288(v=vs.85).aspx
# Receive-Job -Name Job5
# Get-Job
}

if ($test) {
    Test-DscConfiguration -Verbose -Path output\EDV_KS_IIS -ComputerName localhost
}

if ($run) {
    Start-DscConfiguration -Path output\EDV_KS_IIS -ComputerName localhost -Wait -Force # -Verbose
}